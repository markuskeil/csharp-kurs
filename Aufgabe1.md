# C# Anfänger kurs

Wir wollen in der ersten aufgabenserie einen Fahrscheinautomaten entwickeln.
Folgende anforderungen gibt es:

1.  3 feste fahrstrecken mit unterschiedlichen fahrkosten
    1.  Strecke von Plumsdorf nach Mulstadt für 4.50€
    2.  Strecke von Plumsdorf nach Humlangs für 3.60€
    3.  Strecke vom Mulstadt nach Humlangs für 9.80€

1.  Der automat nimmt folgende münzen entgegen: 2€, 1€, 50ct, 20ct
1.  Der Automat gibt wechselgeld in den gleichen münzen zurück
1.  Der automat erzeugt fahrkarten und gibt diese zeilenweise auf dem bildschirm aus


## 1. Aufgabe

Schreibe in deinen eigenen worten alle schritte auf die der automat den kauf eines fahrscheines abarbeiten muss.

## 2. Aufgabe

Nimm deine schritte aus aufgabe 1 und programmiere eine lösung.

Tips:

    -   nutze `if` zur auswahl der strecke.
    -   nutze `while` für das wechselgeld


## 3. Aufgabe

-   Erweitere dein Programm um die möglichkeit mehrere fahrkarten gleichzeitig zu kaufen.
